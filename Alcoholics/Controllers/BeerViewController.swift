//
//  BeerViewController.swift
//  Alcoholics
//
//  Created by Alyson Freitas on 24/11/2018.
//  Copyright © 2018 Alyson Freitas. All rights reserved.
//

import UIKit

class BeerViewController: UIViewController {

    @IBOutlet var name: UILabel?
    @IBOutlet var img: UIImageView?
    @IBOutlet var alcohol: UILabel?
    @IBOutlet var bitter: UILabel?
    @IBOutlet var tagline: UILabel?
    @IBOutlet var desc: UILabel?

    private var beer: Beer?

    func set(beer: Beer){
        self.beer = beer
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        load()
    }
    
    func load() {
        guard self.beer != nil else {
            return
        }
        let beer = self.beer!

        name?.text = beer.name
        alcohol?.text = String.init(format: "%@%@", beer.alcoholLevel!, "%")
        bitter?.text = String.init(format: "Bitterness Scale: %@", beer.bitternessScale!)
        tagline?.text = beer.tagline
        desc?.text = beer.desc
        if beer.imageURL != nil, let imageURL = URL(string: beer.imageURL!) {
            img?.load(url: imageURL, placeholder: UIImage.init(named: "beer")!)
        }
    }

}
