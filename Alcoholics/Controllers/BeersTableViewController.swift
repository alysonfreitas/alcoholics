//
//  BeersTableViewController.swift
//  Alcoholics
//
//  Created by Alyson Freitas on 23/11/2018.
//  Copyright © 2018 Alyson Freitas. All rights reserved.
//

import UIKit
import PKHUD
class BeersTableViewController: UITableViewController {

    var selected: Beer?
    var beers: [Beer] = []
    let model = BeerModel.init()

    override func viewDidLoad() {
        super.viewDidLoad()
        load(showLoading: true)
    }

    func load(showLoading: Bool) {
        if showLoading { HUD.show(.progress) }

        model.getBeers(completionHandler: { (worked, beers) in
            HUD.hide()
            if worked {
                self.beers.append(contentsOf: beers)
                self.tableView.reloadData()
            }
        })
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "beer-cell") as! BeerTableViewCell
        cell.load(beer: beers[indexPath.row])
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selected = beers[indexPath.row]
        self.performSegue(withIdentifier: "beer-details", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as! BeerViewController).set(beer: selected!)
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            load(showLoading: false)
        }
    }
}
