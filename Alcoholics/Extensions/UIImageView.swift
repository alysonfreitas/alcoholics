//
//  UIImageView.swift
//  Alcoholics
//
//  Created by Alyson Freitas on 25/11/2018.
//  Copyright © 2018 Alyson Freitas. All rights reserved.
//

import Shimmer
import Kingfisher
extension UIImageView {
    func shimmerView() -> FBShimmeringView {
        guard let superview = superview else { fatalError("UIImageView must to be wrapped") }

        let shimmeringView = FBShimmeringView(frame: superview.bounds)

        superview.addSubview(shimmeringView)
        shimmeringView.contentView = self
        shimmeringView.isShimmering = true

        return shimmeringView
    }

    func load(url: URL, placeholder: UIImage) {
        let shimmer = shimmerView()

        kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.50))], progressBlock: nil) { (img, err, cacheType, url) in
            shimmer.isShimmering = false
        }
    }
}
