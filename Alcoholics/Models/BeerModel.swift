//
//  BeerModel.swift
//  Alcoholics
//
//  Created by Alyson Freitas on 23/11/2018.
//  Copyright © 2018 Alyson Freitas. All rights reserved.
//

import UIKit

class BeerModel: NSObject {

    private var loading = false
    private var page = 1
    private var lastPage = false

    func getBeers(completionHandler: (@escaping (Bool, [Beer]) -> Void)) {
        if loading || lastPage {
            return
        }
        loading = true
        PunkAPI.getBeers(page: page) { (worked, beers) in
            self.loading = false

            if worked {
                if beers.count == 0 {
                    self.lastPage = true
                }
                self.page += 1
            }

            completionHandler(worked, beers)
        }
    }
}
