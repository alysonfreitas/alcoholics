//
//  PunkAPI.swift
//  Alcoholics
//
//  Created by Alyson Freitas on 25/11/2018.
//  Copyright © 2018 Alyson Freitas. All rights reserved.
//

import UIKit
import Alamofire
import EVReflection
class PunkAPI: NSObject {

    class endPoint {
        static let root = "https://api.punkapi.com/v2"
        static let beers = "/beers?page=%i&per_page=%i"
    }

    static func getBeers(page: Int, completionHandler: (@escaping (Bool, [Beer]) -> Void)) {
        let pageSize = 50
        let url = String.init(format: endPoint.root + endPoint.beers, page, pageSize)
        Alamofire.request(url).responseJSON { response in
            if let data = response.data, let text = String(data: data, encoding: .utf8) {
                let result = [Beer](json: text)
                completionHandler(true, result)
            }
            else {
                completionHandler(false, [])
            }
        }
    }

}
