//
//  BeerTableViewCell.swift
//  Alcoholics
//
//  Created by Alyson Freitas on 24/11/2018.
//  Copyright © 2018 Alyson Freitas. All rights reserved.
//

import UIKit
class BeerTableViewCell: UITableViewCell {

    @IBOutlet var name: UILabel?
    @IBOutlet var img: UIImageView?
    @IBOutlet var alcohol: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func load(beer: Beer) {
        name?.text = beer.name
        alcohol?.text = String.init(format: "%@%@", beer.alcoholLevel!, "%")
        if beer.imageURL != nil, let imageURL = URL(string: beer.imageURL!) {
            img?.load(url: imageURL, placeholder: UIImage.init(named: "beer")!)
        }
    }
    
}
