//
//  Beer.swift
//  Alcoholics
//
//  Created by Alyson Freitas on 23/11/2018.
//  Copyright © 2018 Alyson Freitas. All rights reserved.
//

import Foundation
import EVReflection
class Beer: EVObject {
    var alcoholLevel: String?
    var bitternessScale: String?
    var desc: String?
    var imageURL: String?
    var name: String?
    var tagline: String?

    override func setValue(_ value: Any!, forUndefinedKey key: String) {
        return
    }

    override func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
        return [(keyInObject: "desc", keyInResource: "description"),
                (keyInObject: "imageURL", keyInResource: "image_url"),
                (keyInObject: "alcoholLevel", keyInResource: "abv"),
                (keyInObject: "bitternessScale", keyInResource: "ibu")]
    }
}
